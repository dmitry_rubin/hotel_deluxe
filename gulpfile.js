'use strict'

var gulp = require('gulp');
var less = require('gulp-less');
var watch = require('gulp-watch');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('less', function(){
	return gulp.src('src/style/**/*.less')
	.pipe(less())
	.pipe(gulp.dest('build/style'));
});

gulp.task('autoprefixer', function () {
	return gulp.src('src/style/**/*.less')
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(gulp.dest('build/style'));
});

gulp.task('watch', function(){
	gulp.watch('src/style/*.less', ['less','autoprefixer']);
});

/*var path = {
	build: {
		html: 'build/,
		js: 'build/js/',
		css: 'build/style'
	},
	src: {
		html: 'src/*.html',
		js: 'src/main.js',
		style: 'src/style.less'
	},
	watch: {
		html: 'src/*.html',
		js: 'src/js/*.js',
		style: 'src/style/*.less'
	},
	clean: './build'
}*/